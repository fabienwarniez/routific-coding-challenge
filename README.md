# coding-challenge-backend

Please start with the [instructions](./instructions.md).

# How to run

```
$ npm install
$ DEBUG=coding-challenge-backend:* npm start
```

```
$ brew install mongodb-community
$ mongod --config /usr/local/etc/mongod.conf
```

```
$ curl http://localhost:3000
{"musics":[{"_id":"5de4721b6f84d52c119e4afe","id":"m1","genres":[...
```

# What does this do

It loads music data from the `data/musics.json` file into a local Mongo DB. The data can then be extracted using the `/` endpoint.

# What I spent time on

About 2 hours:

- create a runnable Express app
- connect to Mongo
- load musics during startup into the DB, where to do that from
- extract music and return it as JSON
- How `Promise` works
- How `export` / `import` / `require` work

About 2 hours:

Seeing how long it would take to actually get the most simple recommendation engine running and tested according to the instructions,
I decided to abandon the implementation and switch my focus to designing and documenting the solution instead.
