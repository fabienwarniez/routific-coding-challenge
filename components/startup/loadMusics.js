var fs = require('fs')
var musicDao = require('../dao/music')

function loadMusics(fileLocation) {
  musicDao.deleteAllMusics()
  .then(function() {
    fs.readFile(fileLocation, 'utf8', function(error, json) {
      if (error) {
        console.error('File read failed: ', error)
        return
      }

      try {
        const musics = JSON.parse(json)

        return Promise.all(
          Object.keys(musics).map(function (id) {
            musicDao.addMusic(id, musics[id])
          })
        )
        .then(function(result) {
          console.log('Successfully loaded musics')
        })
      } catch (e) {
        console.error(e)
      }
    })
  })
}

module.exports = loadMusics
