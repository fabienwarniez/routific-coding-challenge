var MongoClient = require('mongodb').MongoClient

function connect(callback) {
  return MongoClient.connect('mongodb://127.0.0.1:27017/')
    .then(client => callback(client.db('local').collection('music')))
    .catch(function(error) {
      console.error(error)

      throw error
    })
}

function getMusics() {
  return connect(musicCollection => musicCollection.find().toArray())
}

function addMusic(id, genres) {
  return connect(musicCollection => musicCollection.insertOne({id, genres}))
}

function deleteAllMusics() {
  return connect(musicCollection => musicCollection.deleteMany())
}

module.exports.getMusics = getMusics
module.exports.addMusic = addMusic
module.exports.deleteAllMusics = deleteAllMusics
