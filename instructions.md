> Please feel free to ask us questions.

Create a README.md file with simple instructions to run it.

The focus of this assignment is on the **web back end** (or **server side**). We will be looking specifically for good practices and your development process. Please use **Node.js with either Express.js, Koa.js or Nest.js** as frameworks. Also, use **either MongoDB or PostgreSQL** for this assignment.

## Fictional MVP!

This fictional client has asked for a recommendation system for his social music player system.
He wants you to essentially take note of what music the user has listened to, which people they follow and from there recommend some songs. There is no like or dislike so far.

In this system there are few "elements";

- **music**: have an ID and a list of tags (see `music.json`)
- **users**: have an ID, follow N other users, have heard Y musics in the past.

How to model or index this data is up to you.

### There should be 3 end points

##### `POST /follow`
Add one follow relationship (see `follows.json`)

the request body have 2 params:
- from: \<user ID\>
- to: \<user ID\>

##### `POST /listen`
Add one song as the user has just listened ( see `listen.json` )

the request body have 2 params:
- user: \<user ID\>
- music: \<music ID\>

##### `GET /recommendations`
Return 5 music recommendations to this user, they should be sorted by relevance

Query string has:
- user: \<user ID\>

response looks like:

```json
{
  "list": ["<music ID>", "<music ID>", "<music ID>", "<music ID>", "<music ID>"]
}
```

--

It's supposed to be a simplistic recommendation engine, which takes into account these main components:
- based on what music they heard before
- people who the user follow in first degree, and maybe even folowees of follows
- maximize for discovery of new songs

#### make it run!

We expect 2 parts:

1. a server that only has business logic (the endpoints) with the DB, it should load `musics.json` upon server start, but other files will be loaded by:
2. a test that runs a series of command to load the data through your endpoints and finally get a recommendation (see `script.md`)

---

#### hints
- there isn't one right answer, but the modelling skills matters
- also, don't worry about finding a perfect solution for the recommendation engine
- Include in your README instructions on how to run the app and test suite -- it would also be nice to include where you focussed on, the things you want to highlight, the thought-process behind the choices you made, the things you've chosen to ignore, etc...

Again, a few things that we will look for in the assignment includes:

- clean code
- good development practices
- good architecture design decisions
- good overall project structure
- good test coverage


Before you start, please read and fill out the first half of [QnA.md](./QnA.md). Once you finish the project, please fill out the second half of [QnA.md](./QnA.md).

That's all for now. Don't hesitate to ask questions, for guidance or for more clarification on the project.

Have fun!
