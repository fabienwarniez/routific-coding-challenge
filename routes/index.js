var musicDao = require('../components/dao/music')

var express = require('express')
var router = express.Router()

router.get('/', function(req, res, next) {
  const musics = musicDao.getMusics().then(function(musics) {
    res.json({ musics: musics })
  })
})

module.exports = router
