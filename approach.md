# Schema and how it's populated

## User collection

| field name | type |
| --- | --- |
| userId | string |
| genres | map of genre -> number of songs listened to in that genre |

| fields | index type |
| --- | --- |
| userId | unique |

Every time a user listens to a song, we fetch their `User` record by id,
and increment by 1 the number of musics they have listened to for that
genre. This would require some locking mechanism on the user record to
prevent race conditions and loss of data.

Ex:

```
userId = 342
genres = {
  folk -> 32
  pop -> 12
  rap -> 24
}
```

User listens to 1 music of genre `pop` + `jazz`:

```
userId = 342
genres = {
  folk -> 32
  pop -> 13
  rap -> 24
  jazz -> 1
}
```

## Listen collection

| field name | type |
| --- | --- |
| userId | string |
| genre | string |
| musicIds | list of music ids that they have listened to in that genre |

| fields | index type |
| --- | --- |
| userId, genre | unique |

Every time a user listens to a song, we fetch the `Listen` record for
all the genres of that given music, and `userId`, then we update the
list. Also would require a locking mechanism.

Ex:

```
userId = 342
genre = folk
musicIds = [3, 6, 7]

userId = 342
genre = pop
musicIds = [3, 8, 9]
```

User listens to music with `musicId` 10 and genres `folk`, `pop`, `rap`:

```
userId = 342
genre = folk
musicIds = [3, 6, 7, 10]

userId = 342
genre = pop
musicIds = [3, 8, 9, 10]

userId = 342
genre = rap
musicIds = [10]
```

## Music collection

| field name | type |
| --- | --- |
| musicId | string |
| genres | list of genre ids |

| fields | index type |
| --- | --- |
| musicId | unique |

We populate this upon server startup from the json file and don't alter
it from then.

## Follow collection

| field name | type |
| --- | --- |
| user1Id | string |
| user2Id | string |

| fields | index type |
| --- | --- |
| user1Id, user2Id | unique |

When a user follows another user, we add a record in that collection.

# Generating the recommendations for user 342

## Fetch all users followed by 342

Fetch their user records which contain their genre preferences.

## Compute an affinity score between user 342 and all followed users

For each `userId` / `genre` combination, we multiply user 342's genre
count with the other user's genre count. This simplistic calculation
gives us a high score when both users have a decent number of listens
for a given genre. But not necessarily if 1 of the 2 has an enormous
appetite for a genre but not the other.

Ex:

```
userId = 342
genres = {
  folk -> 32
  pop -> 12
  rap -> 24
}

userId = 986
genres = {
  folk -> 1
  pop -> 10
  jazz -> 24
}

userId = 564
genres = {
  rap -> 2
}
```

The resulting list of scores looks like:

| userId | genre | score |
| --- | --- | --- |
| 986 | folk | 32 |
| 986 | pop | 120 |
| 986 | rap | 0 |
| 986 | jazz | 0 |
| 564 | folk | 0 |
| 564 | pop | 0 |
| 564 | rap | 48 |

Sorted, and 0s filtered out

| userId | genre | score |
| --- | --- | --- |
| 986 | pop | 120 |
| 564 | rap | 48 |
| 986 | folk | 32 |

This tells us that we should look for pop musics that user 986 has
listened to.

## Look for songs

We now fetch the `Listen` record for user 986, genre `pop`. This would
give us something like:

```
userId = 986
genre = pop
musicIds = [89, 439, 21, ...]
```

as well as the `Listen` record for user 342:

```
userId = 342
genre = pop
musicIds = [401, 439, 12, ...]
```

We can take the 5 first music ids from 986's record that are not present
in 342's record. If there are fewer than 5, then we can repeat the same
process for the second affinity record: 564's rap record, and so on.
