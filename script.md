This test suite should run some command:

- feed the `follows.json` through your `/follow` endpoint, in sequence (or parallel)
- the same for `/listen`
- make a call to user "a" on `/recommendations` and display the results

We usually use Mocha on Node.js.
